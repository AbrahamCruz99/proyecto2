package estructurasdecontrol;

import java.io.IOException;
import java.util.Scanner;

public class TrianguloPitagorico {

	public static void main(String[] args) throws IOException
	{
		for(int linea=1; linea <= 3; linea++) 
		{
			for(int contCa=1; contCa<=linea; contCa++ )
			{
				System.out.print("P");
			}
			System.out.println();
		}
		for(int linea=1; linea <= 3; linea++) 
		{
			for(int contCa=2; contCa>=linea; contCa-- )
			{
				System.out.print("P");
			}
			System.out.println();
		}
	}

}
